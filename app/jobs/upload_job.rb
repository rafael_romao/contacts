# frozen_string_literal: true

require 'csv'

class UploadJob < ApplicationJob
  queue_as :default

  def perform(file, user)
    contact_file = user.contact_files.new(name: file.original_filename, status: ContactFile.statuses[:processing])
    contact_file.save

    CSV.foreach(file.path, col_sep: ',', headers: true, header_converters: :symbol) do |row|
      contact = user.contacts.new(contact_file: contact_file)
      contact.email = row[:email]
      contact.name = row[:name]
      contact.date_of_birth = Date.parse(row[:date_of_birth])
      contact.phone = row[:phone]
      contact.address = row[:address]
      contact.credit_card = row[:credit_card]
      contact.franchise = row[:franchise]

      unless contact.save
        errors = ''
        contact.errors.messages.each{ |m| errors += m[0].to_s + ' ' + m[1].join(' ') + ", " }
        log = user.logs.new(contact_file: contact_file, description: errors)
        log.save
      end
    end

    status = contact_file.contacts.count.positive? ? ContactFile.statuses[:finished] : ContactFile.statuses[:failed]

    contact_file.update(status: status)
  end
end
