# frozen_string_literal: true

class AuthToken
  SECRET_KEY = Rails.application.secrets.secret_key_base.to_s

  def self.encode(data)
    exp = Time.now + 24.hours.to_i
    data[:exp] = exp.to_i

    JWT.encode(data, SECRET_KEY)
  end

  def self.decode(token)
    decoded = JWT.decode(token, SECRET_KEY)[0]
    HashWithIndifferentAccess.new decoded
  end
end
