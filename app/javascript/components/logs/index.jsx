import React, { useState, useEffect, useMemo } from 'react'

const Logs = () => {
  const [logs, setLogs] = useState([]);

  const logsBody = useMemo(() => {
    return logs.map((log, i) => {
      return (
        <tr key={i}>
          <td>{log.id}</td>
          <td>{log.description}</td>
          <td>{log.created_at}</td>
        </tr>
      )
    });
  }, [logs]);

  useEffect(() => {
    loadLogs()
  }, []);

  const loadLogs = () => {
    fetch('/api/v1/logs', {
      method: 'GET',
      headers: {
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content"),
        'Authorization': localStorage.getItem("token")
      }
    })
    .then(res => { return res.json(); })
    .then(data => {
      setLogs(data);
    });
  };

  return (
    <>
      <div className="section-header">
        <span>
          Log of Contacts couldn't imported
        </span>
      </div> 
      
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Erro</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
          {logsBody}
        </tbody>
      </table>
    </>
  )
}

export default Logs;
