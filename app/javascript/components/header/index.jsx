import React from 'react'

const Header = (props) => {
  const links = [
    {
      name: 'Contacts',
      link: '/contacts'
    },
    {
      name: 'Files uploaded',
      link: '/contact_files'
    },
    {
      name: 'Logs',
      link: '/logs'
    }
  ];

  const userActions = [
    {
      name: 'Sign out',
      logout: {logout}
    }
  ];

  const pages = links.map((link, i) => {
    return(
      <a href={link.link} key={i}>
        <p>{link.name}</p>
      </a>
    )
  });

  const logout = (event) => {
    event.preventDefault();

    localStorage.removeItem("token");
    window.location = "/";
  };

  const actions = userActions.map((link, i) => {
    return(
      <a key={i} onClick={logout}>
        <p>Sign out</p>
      </a>
    )
  });

  return (
    <div className="header">
      <div className="header-logo">
        <a className="brand" href="/">
          <img className="brand-logo" alt="Contacts" title="Contacts" src="/logo_name.svg"/>
        </a>
      </div>
      <div className="header-nav">
        {localStorage.getItem("token") && pages}
      </div>
      <div className="header-actions">
        {localStorage.getItem("token") && actions}
      </div>
    </div>
  )
};

export default Header;
