import React, { useState, useEffect, useMemo } from 'react'

const Contacts = () => {
  const [contacts, setContacts] = useState([]);

  const contactsBody = useMemo(() => {
    return contacts.map((contact, i) => {
      return (
        <tr key={i}>
          <td>{contact.id}</td>
          <td>{contact.name}</td>
          <td>{contact.email}</td>
          <td>{contact.date_of_birth}</td>
          <td>{contact.phone}</td>
          <td>{contact.address}</td>
          <td>{contact.credit_card}</td>
          <td>{contact.franchise}</td>
        </tr>
      )
    });
  }, [contacts]);

  useEffect(() => {
    loadContacts()
  }, []);

  const loadContacts = () => {
    fetch('/api/v1/contacts', {
      method: 'GET',
      headers: {
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content"),
        'Authorization': localStorage.getItem("token")
      }
    })
    .then(res => { return res.json(); })
    .then(data => {
      setContacts(data);
    });
  };

  const uploadFile = () => {
    if(document.getElementById('file').files.length < 1) {
      return ;
    }

    const formData = new FormData();
    formData.append('file', document.getElementById('file').files[0], document.getElementById('file').files[0].name);
  
    fetch('/api/v1/contacts', {
      method: 'POST',
      headers: {
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content"),
        'Authorization': localStorage.getItem("token")
      },
      body: formData
    })
    .then(res => { return res.json(); })
    .then(data => {
      toastr.success("We are processing your contact file!")
    })
    .catch(error=> {
      toastr.error("Empty file not accept!")
    });
  };

  return (
    <>
      <div className="section-header">
        <span>
          Upload your contacts files
        </span>
        <div>
          <input type="file" id="file" className="action-file" accept=".csv,.xlsx,.xls, .tab"/>
          <button onClick={uploadFile} className="action-file btn">
            Upload!
          </button>
        </div>
      </div> 
      
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date of Birth</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Credit Card</th>
            <th>Franchise</th>
          </tr>
        </thead>
        <tbody>
          {contactsBody}
        </tbody>
      </table>
    </>
  )
}

export default Contacts;
