import React, { useState, useEffect, useMemo } from 'react'

const ContactFiles = () => {
  const [contactFiles, setContactFiles] = useState([]);

  const contactFilesBody = useMemo(() => {
    return contactFiles.map((contactFile, i) => {
      return (
        <tr key={i}>
          <td>{contactFile.id}</td>
          <td>{contactFile.name}</td>
          <td>{contactFile.status}</td>
          <td>{contactFile.created_at}</td>
        </tr>
      )
    });
  }, [contactFiles]);

  useEffect(() => {
    loadContactFiles()
  }, []);

  const loadContactFiles = () => {
    fetch('/api/v1/contact_files', {
      method: 'GET',
      headers: {
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content"),
        'Authorization': localStorage.getItem("token")
      }
    })
    .then(res => { return res.json(); })
    .then(data => {
      setContactFiles(data);
    });
  };

  return (
    <>
      <div className="section-header">
        <span>
          Files already uploaded
        </span>
      </div> 
      
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Status</th>
            <th>Date Uploaded</th>
          </tr>
        </thead>
        <tbody>
          {contactFilesBody}
        </tbody>
      </table>
    </>
  )
}

export default ContactFiles;
