import React, { useState } from 'react'

const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const login = () => {
    if(!email || !password) {
      return ;
    }

    fetch('/api/v1/sessions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content")
      },
      body: JSON.stringify({ 
        email: email,
        password: password
      })
    })
    .then(res => { return res.json() })
    .then(data => {
      if(data && data.errors) {
        toastr.error("Please review your informations and try again!");
      }
      else {
        localStorage.setItem("token", data.token);
        toastr.success("User successfully logged in!");
        setTimeout(() => { window.location = "/contacts" }, 3000);
      }
    })
    .catch(error => {
      toastr.error("Something wrong, try again!");
    });
  };

  return (
    <div className="container">
      <div className="login">
        <h2>Log in</h2>

        <div id="login_user">
          <div className="field m-25">
            <input autoFocus="autofocus" placeholder="Email" type="email" id="user_email" onChange={e => setEmail(e.target.value)}/>
          </div>
          <div className="field m-25">
            <input placeholder="Password" type="password" id="user_password" onChange={e => setPassword(e.target.value)}/>
          </div>
          <div className="actions m-25">
            <input type="submit" name="commit" className="btn" onClick={login}/>
          </div>
        </div>
        <div className="m-25">
          <a className="link" href="/sign_up">Sign up</a><br/>
        </div>
      </div>
    </div>
  )
};

export default Login;
