import React, { useState } from 'react'

const Signup = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');

  const confirm = () => {
    if(!email || !password || !confirmPassword) {
      return ;
    }

    fetch('/api/v1/users', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-CSRF-Token': document.querySelector("meta[name='csrf-token']").getAttribute("content")
      },
      body: JSON.stringify({ 
        email: email,
        password: password,
        password_confirmation: confirmPassword
      })
    })
    .then(res => { return res.json(); })
    .then(data => {
      if(data && data.errors) {
        toastr.error("Please review your informations and try again!");
      }
      else {
        toastr.success("User successfully register!");
        setTimeout(() => { window.location = "/" }, 3000);
      }
    })
    .catch(error=> {
      toastr.error("Something wrong, try again!");
    });
  };

  return (
    <div className="container">
      <div className="login">
        <h2>Sign up</h2>

        <div id="signup_user">
          <div className="field m-25">
            <input autoFocus="autofocus" placeholder="Email" type="email" onChange={e => setEmail(e.target.value)}/>
          </div>
          <div className="field m-25">
            <input placeholder="Password" type="password" onChange={e => setPassword(e.target.value)}/>
          </div>
          <div className="field m-25">
            <input placeholder="Confirm Password" type="password" onChange={e => setconfirmPassword(e.target.value)}/>
          </div>
          <div className="actions m-25">
            <input type="submit" name="commit" className="btn" onClick={confirm}/>
          </div>
        </div>
        <div className="m-25">
          <a className="link" href="/">Log in</a><br/>
        </div>
      </div>
    </div>
  )
};

export default Signup;
