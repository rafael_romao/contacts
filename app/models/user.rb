# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  has_many :contact_files
  has_many :contacts
  has_many :logs

  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, presence: true
end
