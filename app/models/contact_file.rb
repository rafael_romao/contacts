# frozen_string_literal: true

class ContactFile < ApplicationRecord
  belongs_to :user
  has_many :contacts

  enum status: { on_hold: 0, processing: 1, failed: 2, finished: 3 }

  validates :name, presence: true
  validates :status, presence: true, inclusion: { in: statuses }
end
