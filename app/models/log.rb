# frozen_string_literal: true

class Log < ApplicationRecord
  belongs_to :user
  belongs_to :contact_file

  validates :description, presence: true
end
