# frozen_string_literal: true

class Contact < ApplicationRecord
  belongs_to :contact_file
  belongs_to :user

  validates :name, :email, :date_of_birth, :phone, :address, :credit_card,
            :franchise, presence: true
end
