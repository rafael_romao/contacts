# frozen_string_literal: true

module AuthConcern
  extend ActiveSupport::Concern

  def authorization
    header = request.headers['Authorization']
    header = header.split(' ').last if header

    begin
      @decoded = JWT.decode(header)
      @current_user = User.find(@decoded[:user_id])
    rescue ActiveRecord::RecordNotFound
      render json: {}, status: :unauthorized
    rescue JWT::DecodeError
      render json: {}, status: :unauthorized
    end
  end
end
