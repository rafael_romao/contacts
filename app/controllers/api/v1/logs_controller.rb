# frozen_string_literal: true

module Api
  module V1
    class LogsController < Api::V1::ApplicationController
      before_action :authorize_request

      def index
        @logs = @current_user.logs

        render json: @logs, status: :ok
      end
    end
  end
end
