# frozen_string_literal: true

module Api
  module V1
    class SessionsController < Api::V1::ApplicationController
      def create
        @user = User.find_by(email: session_params[:email])

        if @user&.authenticate(session_params[:password])
          data = { user_id: @user.id }
          token = AuthToken.encode(data)

          render json: { token: token }, status: :ok
        else
          render json: { errors: 'unauthorized' }, status: :unauthorized
        end
      end

      private

      def session_params
        params.permit(:email, :password)
      end
    end
  end
end
