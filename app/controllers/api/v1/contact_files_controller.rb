# frozen_string_literal: true

module Api
  module V1
    class ContactFilesController < Api::V1::ApplicationController
      before_action :authorize_request

      def index
        @contact_files = @current_user.contact_files

        render json: @contact_files, status: :ok
      end
    end
  end
end
