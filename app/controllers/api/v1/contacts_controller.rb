# frozen_string_literal: true

module Api
  module V1
    class ContactsController < Api::V1::ApplicationController
      before_action :authorize_request

      def index
        @contacts = @current_user.contacts.order(name: :desc)

        render json: @contacts, status: :ok
      end

      def create
        UploadJob.perform_now(contacts_params[:file], @current_user)

        render json: {}, status: :ok
      end

      private

      def contacts_params
        params.permit(:file)
      end
    end
  end
end
