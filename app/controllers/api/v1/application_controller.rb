# frozen_string_literal: true

module Api
  module V1
    class ApplicationController < ActionController::Base
      def authorize_request
        header = request.headers['Authorization']
        header = header.split(' ').last if header
        begin
          @decoded = AuthToken.decode(header)
          @current_user = User.find(@decoded[:user_id])
        rescue ActiveRecord::RecordNotFound
          render json: {}, status: :unauthorized
        rescue JWT::DecodeError
          render json: {}, status: :unauthorized
        end
      end
    end
  end
end
