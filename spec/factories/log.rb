# frozen_string_literal: true

FactoryBot.define do
  factory :log do
    description { Faker::String.random }
  end
end
