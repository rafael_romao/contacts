# frozen_string_literal: true

FactoryBot.define do
  factory :contact do
    name { Faker::String.random }
    email { Faker::String.random }
    date_of_birth { DateTime.now }
    phone { Faker::String.random }
    address { Faker::String.random }
    credit_card { Faker::String.random }
    franchise { Faker::String.random }
    contact_file
  end
end
