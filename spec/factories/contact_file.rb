# frozen_string_literal: true

FactoryBot.define do
  factory :contact_file do
    name { Faker::Name.name }
    user
    status { 0 }
  end
end
