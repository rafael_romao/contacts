# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SignUpController, type: :controller do
  describe 'GET#index' do
    it 'should have a Ok response' do
      get :index
      expect(response).to have_http_status(:ok)
    end
  end
end
