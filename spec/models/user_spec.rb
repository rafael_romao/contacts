# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build_stubbed(:user) }

  describe 'associations' do
    it { expect(user).to have_many(:contact_files) }
    it { expect(user).to have_many(:contacts) }
    it { expect(user).to have_many(:logs) }
  end

  describe 'validations' do
    it { expect(user).to validate_presence_of(:email) }
    it { expect(user).to have_secure_password }
  end

  describe 'uniqueness' do
    let(:user) { create(:user) }

    it { expect(user).to validate_uniqueness_of(:email) }
  end
end
