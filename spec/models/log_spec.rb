# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ContactFile, type: :model do
  let(:log) { build_stubbed(:log) }

  describe 'associations' do
    it { expect(log).to belong_to(:user) }
    it { expect(log).to belong_to(:contact_file) }
  end

  describe 'validations' do
    it { expect(log).to validate_presence_of(:description) }
  end
end
