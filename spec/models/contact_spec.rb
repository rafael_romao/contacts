# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Contact, type: :model do
  let(:contact) { build_stubbed(:contact) }

  describe 'associations' do
    it { expect(contact).to belong_to(:contact_file) }
  end

  describe 'validations' do
    it { expect(contact).to validate_presence_of(:name) }
    it { expect(contact).to validate_presence_of(:email) }
    it { expect(contact).to validate_presence_of(:date_of_birth) }
    it { expect(contact).to validate_presence_of(:phone) }
    it { expect(contact).to validate_presence_of(:address) }
    it { expect(contact).to validate_presence_of(:credit_card) }
    it { expect(contact).to validate_presence_of(:franchise) }
  end
end
