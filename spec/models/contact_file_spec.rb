# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ContactFile, type: :model do
  let(:contact_file) { build_stubbed(:contact_file) }

  describe 'associations' do
    it { expect(contact_file).to belong_to(:user) }
    it { expect(contact_file).to have_many(:contacts) }
  end

  describe 'validations' do
    it { expect(contact_file).to validate_presence_of(:name) }
    it { expect(contact_file).to define_enum_for(:status).with_values(%w[on_hold processing failed finished]) }
  end
end
