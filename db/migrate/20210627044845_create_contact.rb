class CreateContact < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.datetime :date_of_birth, null: false
      t.string :address, null: false
      t.string :phone
      t.string :credit_card
      t.string :franchise

      t.references :contact_file
      t.references :user
      t.timestamps

      t.index %i[user_id email], unique: true
    end
  end
end
