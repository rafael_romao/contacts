class CreateLog < ActiveRecord::Migration[6.0]
  def change
    create_table :logs do |t|
      t.string :description, null: false

      t.references :user
      t.references :contact_file
      t.timestamps
    end
  end
end
