class CreateContactFiles < ActiveRecord::Migration[6.0]
  def change
    create_table :contact_files do |t|
      t.string :name, null: false
      t.string :download_link
      t.integer :status, default: 0, null: false

      t.references :user
      t.timestamps
    end
  end
end
