# README

This project was develop using the most recently tecnhologies to help developers run with less force.

## Runing with docker
Please follow the steps to run docker(I can't imagine a world without docker :) ) who will install dependencies and database
with less commands

If do you have permission log error `run sudo chown -R $USER:$USER .` to give you permissions on files

To see how install docker go to
https://docs.docker.com/engine/install/

After install docker run the foward commands
* docker-compose build
* docker-compose run web bundle install
* docker-compose run web yarn install
* docker-compose run web rake db:drop db:create db:migrate db:seed
* docker-compose up

Forward some commands to be confortable

Will list all running containers
* docker container ls 

Will log on container console(If you like run some commands on rails console like me this will help you)
* docker exec -it {container_id} /bin/bash

I love debug if you too, use this command to attach open console to "hearing" byebug and be happy
* docker attach {container_id}

## Runing without docker

To see how to install Ruby on Rails go to, on web site you can choose your system(Ubuntu, Windows, MacOS)
https://gorails.com/setup/ubuntu/21.04

After install run the foward commands
* bundle install
* yarn install
* rake db:drop db:create db:migrate db:seed

And finally to run the project
* rails serve or rails s

To tun the tests should run
* rspec ./spec

## System structure

* Our struct consist on Ruby on Rails on backend, I'm creating Api's to allow the frontend which I'm using React to connect with the backend, I'm not using React as SPA
* I'm versioning the API in API::V1 modules, if one day I needed create a new version this dont't will broke our first version (hypothetically)

* Development
    Authentication: To authenticate the user, I'm using the JWT to valid the access of the user to the system and identity who the user is, I have created my own authenticate with JWT instead of use Devise(Devise is good and can help us a lot because it creates all authentication system but like is a test I prefer to show my abilities using something different than usual).
* Test
    To test our application I'm using rspec, shoulda-matcher, factory_bot and faker.

* Database
    I'm using PostgreSql


## Observation
* Files for thest is on files folder
* Application is running on heroku 

https://contacts-by-rafael.herokuapp.com
