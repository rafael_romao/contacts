Rails.application.routes.draw do
  # Api's routes
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :users, only: %i[create]
      resources :sessions, only: %i[create]
      resources :signup, only: %i[create]
      resources :contacts, only: %i[index create]
      resources :contact_files, only: %i[index]
      resources :logs, only: %i[index]
    end
  end

  resources :sign_up, only: %i[index]
  resources :contacts, only: %i[index]
  resources :contact_files, only: %i[index]
  resources :logs, only: %i[index]

  # Defult route
  root to: 'home#index'
end
